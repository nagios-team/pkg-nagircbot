Type:
./nagircbot -h
for a list of options.

Send a private message to nagircbot for:
 - 'statistics' - returns the number criticals/warnings/etc
 - 'reload' - force reloads the nagios status. use this when you altered the
   nagios configuration
 - 'check' - check if nagios is still running
 - 'resend' - resends the problems
 - 'help' - returns the possible commands


For everything more or less related to 'nagircbot', please feel free
to contact me on: folkert@vanheusden.com
Consider using PGP. My PGP key-id is: 0x1f28d8ae

Please support my opensource development: http://www.vanheusden.com/wishlist.php
