#include "ssl.h"

int
ssl_handshake( server_t *server ){
  SSL_METHOD *meth;
  SSL_CTX *ctx;
  BIO *sbio;
  SSL_library_init();
  meth=SSLv23_method();
  ctx=SSL_CTX_new(meth);


  server->ssl = SSL_new(ctx);
  sbio=BIO_new_socket(server->fd, BIO_NOCLOSE);
  SSL_set_bio(server->ssl, sbio, sbio);
 
  return SSL_connect( server->ssl );
}

void
ssl_close( server_t *server ){

  SSL_shutdown( server->ssl );
  SSL_free(server->ssl);
}


