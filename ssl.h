/* (C) 2006-2010 by folkert@vanheusden.com GPLv2 applies */

#ifndef _SSL_H_
#define _SSL_H_

#include <openssl/ssl.h>

#include <stdint.h>

typedef struct server {
  char      *addr;
  SSL       *ssl;
  int       fd;
    
} server_t;


extern int ssl_handshake( server_t *server );
extern void ssl_close( server_t *server );
#endif

