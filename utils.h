/* (C) 2006-2010 by folkert@vanheusden.com GPLv2 applies */

extern "C" {
  #include "ssl.h"
}

void * mymalloc(int size, char *what);
void * myrealloc(void *oldp, int newsize, char *what);
char * mystrdup(char *in);
char * mtstrndup(char *in, int len);
void myfree(void *p, char *what);
ssize_t WRITE(int fd, char *whereto, size_t len);
ssize_t IRCWRITE( server_t server_conn, char *whereto, size_t len);
int IRCREAD( server_t server_conn, char *buff, size_t len);
 
int get_filesize(char *filename);
time_t get_filechanged(char *filename);
void resolve_host(char *host, struct sockaddr_in *addr);
int connect_to(char *hoststr);
int write_pidfile(char *fname);

#define incopy(a)       *((struct in_addr *)a)

#define max(x, y)	((x) > (y) ? (x) : (y))
#define min(x, y)	((x) < (y) ? (x) : (y))
