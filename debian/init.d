#!/bin/sh

# Example init.d script with LSB support.
# http://refspecs.freestandards.org/LSB_2.1.0/LSB-generic/LSB-generic/iniscrptact.html
#
# Please read this init.d carefully and modify the sections to
# adjust it to the program you want to run.
#
# Copyright (c) 2007 Javier Fernandez-Sanguino <jfs@debian.org>
# Copyright (c) 2010 John Morrissey <jwm@horde.net>
#
# This is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian operating system, in /usr/share/common-licenses/GPL; if
# not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307 USA

### BEGIN INIT INFO
# Provides:          nagircbot
# Required-Start:    $network $remote_fs
# Required-Stop:     $network $remote_fs
# Should-Start:
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: IRC bot that announces Nagios status
# Description:       An IRC bot that reads Nagios' status information and
#                    emits alerts to an IRC channel. It can filter alerts
#                    based on severity (CRITICAL, HARD, SOFT, and/or
#                    UNKNOWN) or by regular expression. It can connect to
#                    IRC servers protected by password or SSL, and can
#                    optionally set the topic to the current Nagios status.
### END INIT INFO

DAEMON=/usr/bin/nagircbot
NAME=nagircbot
DESC=nagircbot

PIDFILE=/var/run/$NAME.pid

if [ ! -x "$DAEMON" ]; then
	exit 0
fi

. /lib/lsb/init-functions

# How long to wait for the server to start, in seconds.
# If set, each time the server is started (on start or restart),
# wait to see if it starts successfully. If not set and the
# daemon takes a while to write its pid file, we might incorrectly
# state that the daemon did not start up.
# This value should always be at least 1.
STARTTIME=1

# How long to wait for the server to stop, in seconds.
# If set too low, some daemons may not stop gracefully
# and 'restart' will not work.
# This value should always be at least 1.
DIETIME=1

# Include defaults if available
if [ -f "/etc/default/$NAME" ]; then
	. "/etc/default/$NAME"
fi

# Check if pid's cmdline matches a given name.
running_pid() {
	local pid=$1
	shift

	local name=$1
	shift

	if [ -z "$pid" ]; then
		return 1
	fi
	if [ ! -d "/proc/$pid" ]; then
		return 1
	fi

	cmd=$(cat "/proc/$pid/cmdline" | tr "\000" "\n" | head -n1 | cut -d: -f1)
	# Is this the expected server?
	if [ "$cmd" != "$name" ]; then
		return 1
	fi

	return 0
}

running() {
	local pid

	# No pidfile, probably no daemon present.
	if [ ! -f "$PIDFILE" ]; then
		return 1
	fi
	pid=$(cat "$PIDFILE")
	if ! running_pid "$pid" "$DAEMON"; then
		return 1
	fi
	return 0
}

start_server() {
	start_daemon -p "$PIDFILE" "$DAEMON" -P "$PIDFILE" \
		-z "$USER" -Xf "$STATUSFILE" \
		-s "$SERVER" -n "$NICK" -c "$CHANNEL" $OPTIONS
}

stop_server() {
	killproc -p "$PIDFILE" "$DAEMON"
}

reload_server() {
	if [ ! -f "$PIDFILE" ]; then
		return 1
	fi
	pid=$(pidofproc "$PIDFILE")
	kill -HUP $pid
	return
}

# Force the process to die. SIGTERM it at first,
# then SIGKILL after $DIETIME.
force_stop() {
	if [ ! -e "$PIDFILE" ]; then
		return 0
	fi
	if ! running; then
		rm -f "$PIDFILE"
		return 0
	fi

	kill -TERM "$pid"
	if ! running; then
		rm -f "$PIDFILE"
		return
	fi

	sleep "${DIETIME:-1}"

	kill -KILL $pid
	sleep "${DIETIME:-1}"
	if ! running; then
		rm -f "$PIDFILE"
		return
	fi

	echo "Cannot kill $NAME (pid=$pid)!"
	return 1
}

case "$1" in
start)
	case $ENABLE in
	[Nn][Oo]|[Ff][Aa][Ll][Ss][Ee])
		exit 0
		;;
	esac

	log_daemon_msg "Starting $DESC " "$NAME"

	if running; then
		log_progress_msg "already running"
		log_end_msg 0
		exit
	fi

	if ! start_server; then
		log_end_msg 1
		exit
	fi

	if ! running; then
		sleep "${STARTTIME:-1}"
	fi
	if ! running; then
		log_end_msg 7
		exit
	fi

	log_end_msg 0
	exit
	;;

stop)
	log_daemon_msg "Stopping $DESC" "$NAME"

	if ! running; then
		log_progress_msg "not running"
		log_end_msg 0
		exit
	fi

	stop_server
	log_end_msg $?
	exit
	;;

force-stop)
	# First, try to gracefully stop the program.
	"$0" stop
	if ! running; then
		exit 0
	fi

	# It's still running; try to kill it more forcefully.
	log_daemon_msg "Stopping (force) $DESC" "$NAME"
	force_stop
	log_end_msg $?
	exit
	;;

restart|force-reload)
	case $ENABLE in
	[Nn][Oo]|[Ff][Aa][Ll][Ss][Ee])
		exit 0
		;;
	esac

	log_daemon_msg "Restarting $DESC" "$NAME"

	if ! running; then
		log_end_msg 0
		exit
	fi

	stop_server
	status=$?
	if [ $status -ne 0 ]; then
		if [ "$1" = 'force-reload' ]; then
			log_end_msg 0
		else
			log_end_msg $status
		fi
		exit
	fi

	# Some servers take time to stop.
	sleep "${DIETIME:-1}"

	start_server
	status=$?
	if [ $status -ne 0 ]; then
		if [ "$1" = "force-reload" ]; then
			log_end_msg 0
		else
			log_end_msg $status
		fi
		exit
	fi

	sleep "${STARTTIME:-1}"

	if ! running; then
		if [ "$1" = "force-reload" ]; then
			log_end_msg 0
		else
			log_end_msg 7
		fi
		exit
	fi

	log_end_msg 0
	exit
	;;

status)
	status_of_proc -p "$PIDFILE" "$DAEMON" "$NAME"
	exit
	;;

# Use this if the daemon cannot reload...
reload)
	log_warning_msg "Reloading $NAME daemon: not implemented, as the daemon"
	log_warning_msg "cannot re-read the config file (use restart)."
	exit 3
	;;
# ... and this if it can.
#reload)
	# If the daemon can reload its config files on the fly
	# (for example, by sending it SIGHUP):
	#log_daemon_msg "Reloading $DESC configuration" "$NAME"
	#if ! running; then
	#	log_progress_msg "not running"
	#	log_end_msg 7
	#	exit
	#fi
	#
	#reload_server
	#if ! running; then
	#	log_progress_msg "died on reload"
	#	log_end_msg 1
	#	exit
	#fi
	#exit 0
	#;;

	# If the daemon responds to changes in its config file
	# automatically, make this a do-nothing entry:
	#exit 0
	#;;

*)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|force-stop|restart|force-reload|status}" >&2
	exit 3
	;;
esac

exit 0
