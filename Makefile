# (C) 2006-2011 by folkert@vanheusden.com GPLv2 applies

VERSION=0.0.33

DEBUG= -g  -D_DEBUG #-fprofile-arcs -ftest-coverage # -pg -g
CXXFLAGS+=-Wall -O2 -DVERSION=\"${VERSION}\" $(DEBUG)
CFLAGS+=${CXXFLAGS}
LDFLAGS+=$(DEBUG) -lcrypto -lssl -lstdc++

OBJS= ssl.o error.o log.o utils.o br.o pl.o anna.o

all: nagircbot

nagircbot: $(OBJS)
	$(CC) -Wall -W $(OBJS) $(LDFLAGS) -o nagircbot

install: nagircbot
	cp nagircbot /usr/local/bin

clean:
	rm -f $(OBJS) nagircbot core *.da *.gcov *.bb*

package: clean
	mkdir nagircbot-$(VERSION)
	cp *.c* *.h Makefile thanks.txt readme.txt license.* nagircbot-$(VERSION)
	tar czf nagircbot-$(VERSION).tgz nagircbot-$(VERSION)
	rm -rf nagircbot-$(VERSION)
